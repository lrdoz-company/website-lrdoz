FROM node:lts-alpine as build-app
LABEL stage=intermediate

WORKDIR /app 

#Install dependency
COPY package.json /app/package.json
RUN npm install

# Buidl app
COPY . /app
RUN npm run-script build

##########################################
FROM nginx:stable-alpine

COPY --from=build-app /app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
