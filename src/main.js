import Vue from 'vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { Plugin } from "vue-fragment"; 

import {router} from '@/router.js'
import i18n from './i18n.js'

import App from '@/pages/app/app.vue'
import Header from '@/components/header/header.vue'
import Footer from '@/components/footer/footer.vue'

import Introduction from '@/components/introduction/introduction.vue'
import Project from '@/components/project/project.vue'

// Add used BootstrapVue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.component("header-component", Header);
Vue.component("footer-component", Footer);

Vue.component("introduction-component", Introduction);
Vue.component("project-component", Project);

Vue.use(Plugin);

const app = new Vue({
  el: "#app",
  render: app => app(App),
  i18n,
  router
});

export { app }