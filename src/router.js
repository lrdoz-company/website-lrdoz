import Vue from "vue";
import VueRouter from "vue-router";

import Content from "@/pages/content/content.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    name: 'Content',
    component: Content,
  }
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export {router};
