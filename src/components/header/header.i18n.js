
export default {
  messages:{
    en: {
      introduction: "Home",
      project: "Projects",
      contact: "Contact"
    },
    fr: {
      introduction: "Accueil",
      project: "Projets",
      contact: "Contact"
    }
  }
}