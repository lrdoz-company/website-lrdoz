import Content from "@/pages/content/content.js"
import header_i18n from "./header.i18n.js"

export default {
    name: 'Header',
    components: {},
    i18n: header_i18n,

    data() {
      return{
        navValues: Content.sections,
        langs: ['fr', 'en'],
        isActive: true,
        showHeader: false
      }
    },

    methods:{
      showNavBar: function(){
        this.isActive = ! this.isActive;
      },

      updateHeader: function(){
        this.showHeader = window.scrollY > 0;
      }
    },
    mounted() {
      document.addEventListener('scroll', this.updateHeader);
    }
  };