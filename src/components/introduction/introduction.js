import introduction_i18n from "./introduction.i18n.js"

export default {
    name: 'Intruduction',
    components: {},
    i18n: introduction_i18n,
    data() {
        return {
            scrollPosition: "50% 0px"
        }
    },
    methods:{
        scrollDown: function(){
            this.scrollPosition =  "50% " + window.scrollY + "px";
        }
    },
    mounted() {
      document.addEventListener('scroll', this.scrollDown);
    }
}