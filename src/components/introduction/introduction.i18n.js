
export default {
  messages:{
    en: {
    },
    fr: {
      Title: "Developpeur",
      Presentation: "Je m'appelle Sami. \
                    Je suis passionné d'informatique en général et j'en maitrise de nombreux domaines.<br/>\
                    Je suis polyvalent, autonome et j'aime innover sur mes projets personnels et professionnels.",
      Linkedin: "Linkedin",
      Git: "Gitlab"
    }
  }
}