import project_i18n from "./project.i18n.js"
import image from "./presentation.jpg"

export default {
    name: 'Project',
    components: {},
    i18n: project_i18n,
    data() {
        return {
            image: image
        }
    },
    methods:{
        getImgUrl: function(path) {
            var images = require.context('@/assets/', false, /\.jpg$/)
            return images(path)
        }
    }
}