
export default {
  messages:{
    en: {
      Title: "Projects Perosnnels",
      Projects: []
    },
    fr: {
      Title: "Projets",
      Projects: [
        {
          Title: 'Sendrop',
          Description: "Application d'upload de fichier.",
          Tools: ["Golang", "Docker", "CI/CD"],
          Picture: "./sendrop.jpg",
          Link: "https://gitlab.com/lrdoz/sendrop-go",
          Url: "https://sendrop.lrdoz.com/"
      },
      {
          Title: 'Skyblock collector',
          Description: "Collect et hébergement d'inforamtion sur les enchaire d'objet du jeux Skyblock et récupération des données en API.",
          Tools: ["Python3", "Pylint", "GraphQL", "Docker", "CI/CD"],
          Picture: "./skyblock.jpg",
          Link: "https://gitlab.com/lrdoz/skyblock",
          Url: "https://skyblock.lrdoz.com/graphql/"
      },
      {
          Title: 'Test Pierre Fabre',
          Description: "Création et résolution d'un test de recrutement pour du développement DevOPS pour l'équipe DAIS pierre fabre.",
          Tools: ["Python3", "FastAPI", "Pylint", "Docker", "docker-compose", "CI/CD"],
          Picture: "./PierreFabre.jpg",
          Link: "https://gitlab.com/lrdoz/pf",
          Url: "https://spacex.lrdoz.com/docs"
      },
      {
        Title: 'Advent of code',
        Description: "Calendrier de l'avent noel, proposant des problèmes algorithmiques. <br/> https://adventofcode.com/",
        Tools: ["Python3", "Pylint", "Pytest", "CI/CD"],
        Picture: "./advent_of_code.jpg",
        Link: "https://gitlab.com/advent-of-code4/2020"
      },
      {
        Title: 'Site personnel',
        Description: "Site de présentation de mes projets personnels réalisés.",
        Picture: './presentation.jpg',
        Tools: ["Vue.js", "Bootstrap", "Docker", "Nginx", "CI/CD"],
        Link: "https://gitlab.com/lrdoz-company/website-lrdoz"
      }
    ]
    }
  }
}