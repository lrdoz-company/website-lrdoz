
export default {
  messages:{
    en: {
      Address: "Contact",
      About: "About",
    },
    fr: {
      Address: "Coordonnées",
      About: 'Réseaux',
    }
  }
}