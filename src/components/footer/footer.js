import footer_i18n from "./footer.i18n.js"

export default {
    name: 'Footer',
    components: {},
    i18n: footer_i18n,
    data() {
      return{
        langs: ['fr', 'en'],
        isActive: true
      }
    },
    methods:{

      
    }
  };
