import VueI18n from 'vue-i18n'
import Vue from 'vue'

function get_default_locale(){
  let storage = localStorage.getItem("locale");
  return storage == undefined ? "fr" : storage;
}

Vue.use(VueI18n)


const i18n = new VueI18n({
  locale: get_default_locale()
})


export default i18n;
