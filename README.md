# Lrdoz WebSite

## Description

This project is an implementation of my website on vue.js

## Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Docker 

Docker image run the website on the nginx server at port 80

```
docker pull registry.gitlab.com/lrdoz-company/website-lrdoz/front
```
